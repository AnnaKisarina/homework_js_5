function CreateNewUser() {
    this.firstName;
    while (!this.firstName || !isNaN(this.firstName)){
        this.firstName = prompt("Please, enter your first name!");
    }
    this.lastName;
    while (!this.lastName || !isNaN(this.lastName)){
        this.lastName = prompt("Please, enter your last name!");
    }

    this.getLogin = function(){
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    };

    Object.defineProperties(this, {
        firstName: {
            writable: false,
        },
        lastName: {
            writable: false,
        }
    });

    this.setFirstName = function(value) {
        Object.defineProperty(
            this,
            "firstName",
            {
                value: value
            }
        );
    };
    this.setLastName = function(value) {
        Object.defineProperty(
            this,
            "lastName",
            {
                value: value
            }
        );
    }
}

let newUser = new CreateNewUser();

/* For test of setter */

/* newUser.setFirstName('Irbis'); */

console.log(newUser.getLogin());





